/// <reference types="Cypress" />
/* global before, context, cy, Cypress */

Cypress.env({
  NODE_TLS_REJECT_UNAUTHORIZED: "0"
})
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"
process.env.NODE_TLS_ACCEPT_UNTRUSTED_CERTIFICATES_THIS_IS_INSECURE = "1"

describe("Firebase RDB Save", () => {
  before(() => {
    // Login using custom token
    cy.login()
    cy.visit("/")
  })

  it("Should save in Firebase Rdb", () => {
    const fakeUserSetting = {
      username: "cypressTestUserName"
    }
    cy.callRtdb("set", "users/008", fakeUserSetting)
  })

  it("Should read from Firebase Rdb", () => {
    cy.callRtdb("get", "users/008").then(user => {
      // Confirm new data has users uid
      cy.wrap(user)
        .its("createdBy")
        .should("equal", Cypress.env("TEST_UID"))
    })
  })
})
