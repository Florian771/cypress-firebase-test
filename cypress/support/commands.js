/// <reference types="Cypress" />
/* global before, context, cy, Cypress */

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

/* FIREBAE */
import firebase from "firebase/app"
import "firebase/auth"
import "firebase/database"
import "firebase/firestore"
import { attachCustomCommands } from "cypress-firebase"

export const fbConfig = {
  apiKey: "AIzaSyCQ0JLQwNkdiqjL94y7rjzJmMMAsEjKPWg",
  authDomain: "cypress-firebase-test.firebaseapp.com",
  databaseURL: "https://cypress-firebase-test.firebaseio.com",
  projectId: "cypress-firebase-test",
  storageBucket: "",
  messagingSenderId: "75111904221"
}

window.fbInstance = firebase.initializeApp(fbConfig)

attachCustomCommands({ Cypress, cy, firebase })

/* CLEANUP */
Cypress.Commands.add("cleanUpXHR", function() {
  if (Cypress.env("run_all_suite")) {
    cy.visit(`${Cypress.env("yourAppUrl")}/404`, { failOnStatusCode: false })
  }
})
