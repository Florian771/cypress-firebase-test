import React, { useState } from "react"
import firebase from "firebase/app"
import "firebase/auth"
import "firebase/database"
import "firebase/firestore"

let firebaseInitialized: boolean = false
const projectId: string = "007"
const fbConfig: any = {
  apiKey: "AIzaSyCQ0JLQwNkdiqjL94y7rjzJmMMAsEjKPWg",
  authDomain: "cypress-firebase-test.firebaseapp.com",
  databaseURL: "https://cypress-firebase-test.firebaseio.com",
  projectId: "cypress-firebase-test",
  storageBucket: "",
  messagingSenderId: "75111904221"
}

function App() {
  const [user, setUser] = useState<any>({})
  const [project, setProject] = useState<any>({})

  // @ts-ignore
  if (!firebaseInitialized && !window.fbInstance) {
    // INITIALIZE
    firebaseInitialized = true
    firebase.initializeApp(fbConfig)

    // GOOGLE AUTH
    var provider = new firebase.auth.GoogleAuthProvider()
    firebase
      .auth()
      .signInWithPopup(provider)
      .then(async function(result: any) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token: string = result.credential.accessToken
        // The signed-in user info.
        var user = result.user
        setUser(user)

        // SAVE IN RDB
        var database: any = firebase.database()
        await database.ref("projects/" + projectId).set({
          name: "Project XY"
        })

        // READ FROM RDB
        database
          .ref("/projects/" + projectId)
          .once("value")
          .then((snapshot: any) => {
            const data: any = snapshot.val()
            setProject(data)
          })
      })
      .catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code
        var errorMessage = error.message
        // The email of the user's account used.
        var email = error.email
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential
        // ...
      })
  }

  return (
    <div>
      App
      <br />
      uid: {user.uid}
      <br />
      project: {JSON.stringify(project)}
    </div>
  )
}

export default App
